
# WE ARE WATCHING YOU !!!


1. ###MANAGING USERS
  *  `rake vk:watch[id]`
    Start *watching* on a user with **id**
  *  `rake vk:watch[id,CODENAME]`
    Start *watching* on a user with **id** and give it a **CODENAME**
  *  `rake vk:unwatch[id]`
    Temporarily stop *watching* on user with **id**

1. ###OBSERVING
  *  `rake vk:list`
    Show list of currently *watched* users with their last status

1. ###MAINTENANCE
  *  `rake vk:doing` - ***DOING THINGS.***
    The magic happens here. Checks status of all *watched* users and stores it to _The DATABASE_
