class AddCodename < ActiveRecord::Migration
  def change
    add_column :accounts, :codename, :string, after: :name
  end
end
