class AddTrackingStatus < ActiveRecord::Migration
  def change
    add_column :accounts, :status, :boolean, after: :name
  end
end
