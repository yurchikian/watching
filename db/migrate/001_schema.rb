class Schema < ActiveRecord::Migration
  def change
    create_table :accounts, force: true do |t|
      t.integer :vk_id
      t.string :name

      t.timestamps
    end


    create_table :records, force: true do |t|
      t.belongs_to :account
      t.boolean :online
      t.boolean :online_mobile

      t.timestamps
    end
  end
end
