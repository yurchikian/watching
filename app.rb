require 'rubygems'
require 'bundler'
require 'json'

Bundler.require(:default)

#set :server, 'thin'

Dir[File.dirname(__FILE__) + "/models/*.rb"].each {|file| require file }


ActiveRecord::Base.logger = Logger.new('debug.log')
configuration = YAML::load(IO.read('config/database.yml'))
ActiveRecord::Base.establish_connection(configuration['development'])

#get '/' do
#  Record.joins(:account).to_json
#end

# VK API endpoint
@api_path = "https://api.vk.com/method/"


def watchUser(id, codename)

  acc = Account.find_by_vk_id(id)

  if id == '0'
    acc = Account.find_by_codename(codename)
  end

  if acc
    if acc.status
      puts 'This user is already in The DATABASE and being watched'
    else
      acc.status = true
      if acc.save
        puts "User #{acc.id} (codename: #{acc.codename ? acc.codename : acc.name}) is now being watched"
      end
    end
  elsif id != '0'
    result = JSON.parse(RestClient.get @api_path + "users.get", {params: {user_ids: id}})
    response = result['response'][0]

    acc = Account.find_by_vk_id(response['uid'])

    if acc
      if acc.status
        puts "This user is already in The DATABASE under it's uid #{acc.vk_id} and being watched"
      else
        acc.status = true
        if acc.save
          puts "User #{acc.id} (codename: #{acc.codename ? acc.codename : acc.name}) is now being watched"
        end
      end
    else

      acc = Account.new
      acc.vk_id = response['uid']
      acc.name = response['first_name'] + ' ' + response['last_name']

      if codename
        acc.codename = codename
      end

      # Start watching the user
      acc.status = true

      if acc.save
        puts "User #{acc.id} (codename: #{acc.codename ? acc.codename : acc.name}) has been added to The DATABASE and now being watched"
      else
        puts "Could not save user to The DATABASE"
      end
    end
  else
    puts "User Not Found"
  end
end


def unwatchUser(id, codename)

  acc = Account.find_by_vk_id id

  if id == '0'
    acc = Account.find_by_codename(codename)
  end

  if acc
    acc.status = false
    acc.save!
    puts "Temporarily stopped watching user #{acc.id} (codename: #{acc.codename ? acc.codename : acc.name})"
  else
    puts 'This user not in The DATABASE'
  end
end


def listWatched()

  Account.all.each {|acc|

    if acc.status
      record_count = acc.records.count
      last_record = acc.records.order(:created_at).last

      if not last_record
        last_record = Record.new
      end

      puts "U: [#{acc.vk_id}]: '#{acc.codename ? acc.codename : acc.name}' records: #{record_count}, last: #{last_record.online ? 'Online' : 'Offline'} @ #{last_record.created_at}"
    end
  }
end


def doThings()
  user_ids = Account.where(:status => true).map{|acc| acc.vk_id}[0..999]

  user_idss = user_ids.join(',')
  result = JSON.parse(RestClient.get @api_path + "users.get", {params: {user_ids: user_idss, fields: 'online, online_mobile'}})

  response = result['response']

  response.each {|res|
    acc = Account.find_by_vk_id res['uid']
    if acc

      rec = Record.new
      rec.online = (res['online'] == 1)
      rec.online_mobile = (res['online_mobile'] == 1) || false

      rec.account = acc

      rec.save!

    end
  }
end
